<Query Kind="Program">
  <Connection>
    <ID>743117b9-9286-4e05-8e28-d61e4db67aa1</ID>
    <Persist>true</Persist>
    <Driver Assembly="Madd0.AzureStorageDriver" PublicKeyToken="47842961fb3025d7">Madd0.AzureStorageDriver.AzureDriver</Driver>
    <DriverData>
      <UseLocalStorage>false</UseLocalStorage>
      <AccountKey>AQAAANCMnd8BFdERjHoAwE/Cl+sBAAAAbExJFUfLdk6KiDBP1QDsGAAAAAACAAAAAAAQZgAAAAEAACAAAACZxBWg1ErOR5lflCER23xOUeefhkgTBUzHJbDjrts0dQAAAAAOgAAAAAIAACAAAADXJmmLrp49oLuTm1qir4m4Zs7iXVxNLDAA/pHhH1PKG2AAAAByzcIX3kExzW1X/zlBp1sthSiWPs2es54XFm9d94qXuJPnc2sQrg9y/jG8CdIwJr6B4uLdVeWKQij+RfMVE7z9qnJm41dIxNFblhPqOmyvWC2gQe/kMgVVPi5ONBEiXtxAAAAAQ9JD2GDzDjgD3Y0JgJViJYCJ7UosIQndXUpjhQj12ORRemYYcM9EVrmxMJ07CBhPXQayV6maf4i9ElPopHL8IA==</AccountKey>
      <AccountName>portalvhds6c3stskcm02</AccountName>
      <UseHttps>true</UseHttps>
    </DriverData>
  </Connection>
  <Output>DataGrids</Output>
</Query>

void Main()
{	
	Initialize();
	// Set basepath to <repository path>\testrig\generated\
	BasePath =  @"C:\code\cloudedjudgement\testrig\generated\";
	OrderIdsFilename = "OrderIds.txt";
	ItemIdsFilename = "ItemIds.txt";
	ItemNamesFilename = "ItemNames.txt";
	
	EssentialRequests.Count.Dump("Essential requests count");
	CreateOrderRequests.Count.Dump("Orders");
	OrderIds.Count().Dump("OrderIds");

	
	// Switch to false if you just want to play around with member proprties/functions
	if (false) {
		
		// A random set of 100 order Ids will be created and saved in orderIds.txt if the file does not exist.
		// Additionally, item Ids and item names from Azure Table Storage will be saved in itemIds.txt 
		// and names.txt if the files do already exist. These files will be used to generate the subsequent requests.
		
		WriteLinesToFile("ScaffoldingRequests.txt", ScaffoldingRequests);
		WriteLinesToFile("CreateOrderRequests.txt", CreateOrderRequests);
		WriteLinesToFile("AddItemRequests.txt", AddItemRequests);
		WriteLinesToFile("SearchRequests.txt", SearchRequests);
		WriteLinesToFile("SubmitItemRequests.txt", SubmitOrderRequests);

		// The SLA parameter of 0 req/sec will generate a bare bones, non optimized set of 
		// requests to test the minimum SLA performance
		WriteTestScriptsForSlas(0, 15, 20, 25, 30, 35, 40, 45, 50, 60, 70, 80, 90, 100);
	}
}

public void Initialize() {
	_createOrderRequestsLazy = new Lazy<List<string>>(() => {
		"Creating requests to create orders ...".Dump();
		var result =  OrderIds.Select (orderId => string.Format("/api/create-order?orderid={0}", orderId)).ToList();
		"... done".Dump();
		return result;
	});
	
	_addItemRequestsLazy =  new Lazy<List<string>>(() => {
		"Creating requests to add items to orders ...".Dump();
		var result = OrderIds
			.SelectMany(orderId =>
				TakeRandomExclusive(ItemIds, 10)
				.SelectMany(itemId =>
					Enumerable.Range(1, Random.Next(1, 3))
				.Select(itemQuauntity => string.Format("/api/add-item?orderId={0}&itemId={1}", orderId, itemId)))).ToList();
		"... done".Dump();
		return result;
	});
		
	_searchRequestsLazy = new Lazy<List<string>>(() => {
		"Creating search requests ...".Dump();
		var result = ItemIds.Concat(ItemNames).Select(itemId => string.Format("/api/search-products?q={0}", itemId)).OrderBy(r => Random.Next()).ToList();
		"... done".Dump();
		return result;
	});
	
	_submitOrderReqeustsLazy = new Lazy<List<string>>(() => { 
		"Creating requests to submit orders ...".Dump();
		var result = OrderIds.Select(orderId => string.Format("/api/submit-order?orderId={0}", orderId)).ToList();
		"... done".Dump();
		return result; 
	});
	
	_orderIdsLazy = new Lazy<List<string>>(() => {
		string.Format("Getting orderIds from '{0}', or creating random ones if they don't exist ...", OrderIdsFilename).Dump();
		var result = GetListFromFileOrElse(OrderIdsFilename, () => Enumerable.Range(1, 5000).Select(i => Guid.NewGuid().ToString()).ToList());
		"... done".Dump();
		return result;
	});
	
	_itemIdsLazy = new Lazy<List<string>>(() => { 
		string.Format("Getting itemIds from '{0}', or getting them from table storage if they don't exist ...", ItemIdsFilename).Dump();
		var result = GetListFromFileOrElse(ItemIdsFilename, () => Products.ToList().Select(p => p.sku.Trim()).ToList());
		"... done".Dump();
		return result;
	});
	
	_itemNamesLazy = new Lazy<List<string>>(() => {
		string.Format("Getting itemNames from '{0}', or getting them from table storage if they don't exist ...", ItemNamesFilename).Dump();
		var result = GetListFromFileOrElse(ItemNamesFilename, () => Products.ToList().SelectMany(p => p.name.Split(' ').Select(name => name.Trim())).ToList());
		"... done".Dump();
		return result;
	});
	
	_essentialRequestsLazy = new Lazy<List<string>>(() => {
		// These essential requests are what is needed to demonstrate:
		// * Search SLA of 10,000 r/s
		// * Add Item SLA of 1000-1500 r/s (yes, higher than called for, but demonstrates adding 10 distinct skus 1-2x on 100 orders)
		// * Submiting orders SLA of 50 r/s
		"Creating essential tests to demonstrate minimum SLAs".Dump();
		var result = TakeAllOrCount(SearchRequests, SearchSla).Concat(AddItemRequests).Concat(SubmitOrderRequests).ToList();
		"... done".Dump();
		return result;
	});
	
	_scaffoldingRequestsLazy = new Lazy<List<string>>(() => {
		"Creating scaffolding requests to be run before the performance test".Dump();
		var result = CreateOrderRequests.Concat(AddItemRequests).Concat(SearchRequests).OrderBy(r => Random.Next()).ToList();
		"... done".Dump();
		return result;
	});	
}

private  const int SearchSla = 10 * 1000;
public Random Random = new Random();

public string BasePath { get; set; }
public string ItemIdsFilename { get; set; }
public string ItemNamesFilename { get; set; }
public string OrderIdsFilename { get; set; }

public List<string> ItemNames { get {return _itemNamesLazy.Value; } }
public List<string> OrderIds { get { return _orderIdsLazy.Value;} }
public List<string> ItemIds { get {return _itemIdsLazy.Value;} }
public List<string> CreateOrderRequests { get { return _createOrderRequestsLazy.Value; } }
public List<string> AddItemRequests { get {return _addItemRequestsLazy.Value; } }
public List<string> SearchRequests { get { return _searchRequestsLazy.Value;} }
public List<string> SubmitOrderRequests { get {return _submitOrderReqeustsLazy.Value; } }
public List<string> ScaffoldingRequests { get {return _scaffoldingRequestsLazy.Value; } }
public List<string> EssentialRequests { get { return _essentialRequestsLazy.Value; } }

public Lazy<List<string>> _createOrderRequestsLazy;
public Lazy<List<string>> _addItemRequestsLazy;
public Lazy<List<string>> _searchRequestsLazy;
public Lazy<List<string>> _submitOrderReqeustsLazy;
public Lazy<List<string>> _orderIdsLazy;
public Lazy<List<string>> _itemIdsLazy;
public Lazy<List<string>> _itemNamesLazy;
public Lazy<List<string>> _essentialRequestsLazy;
public Lazy<List<string>> _scaffoldingRequestsLazy;


public void WriteTestScriptsForSlas(params int[] safeExpectedSlaInThousands) {
	safeExpectedSlaInThousands.Select (slaInThousands => slaInThousands * 1000).ToList().ForEach(safeExpectedSlaCapacity => {
		// We can backfill the essential requests with additional submit item requests, adding to the number
		// of requests we expect to make in a second (safeExpectedSlaCapacity). If safeExpectedSlaCapacity is 0
		// or less than the essential reques
		var backFillCount = safeExpectedSlaCapacity - EssentialRequests.Count;
		var allRequests = 
		// If we have room to backfill
		backFillCount > 0 ?
			// Then generate more submitOrderRequests and add them to the essential requests, 
			TakeAllOrCount(SubmitOrderRequests, backFillCount).Concat(EssentialRequests).ToList() 
			// Otherwise, just use the essential requests
			: EssentialRequests;
	
		// Randomize requests
		allRequests = allRequests.OrderBy (r => Random.Next()).ToList();
		string.Format("Successfully created test requests optimized for expected SLA of {0} req/sec.", safeExpectedSlaCapacity).Dump();
	 	WriteLinesToFile(string.Format("TestRequests{0}.txt", safeExpectedSlaCapacity), allRequests);
	});
}

// This method returns a collection the size of 'count', or the original 'collection', if it is bigger.
// It will backfill any additional items from the original source collection.
public List<string> TakeAllOrCount(List<string> collection, int count) {
	if (collection.Count > count) {
		return collection.ToList();
	}
	// how many sets of the original collection will wee need to fill requested count?
	var multiplier = (int)Math.Ceiling((double)count / (double)collection.Count);
	return Enumerable.Range(0, multiplier)
		.SelectMany(x => collection.ToList())
		.Take(count)
		.ToList();
}

// 
public List<string> TakeRandomExclusive(List<string> collection, int count) {
	return Enumerable.Range(0, collection.Count).OrderBy (index => Random.Next()).Select (index => collection[index]).Take(count).ToList();
}


public void WriteLinesToFile(string filename, List<string> input) {
	var fullPath = BasePath + filename;
    if (File.Exists(fullPath)) {
    	File.Delete(fullPath);
	}
	
	using (FileStream fs = File.Create(fullPath)) {
		input.ForEach(s => {
		    var info = new UTF8Encoding(true).GetBytes(s + "\n");
			fs.Write(info, 0, info.Length);
		});
    }
	string.Format("Created new file: {0}", fullPath).Dump();

}

public List<string> ReadTrimmedLinesFromFile(string filename) {
	var fullPath = BasePath + filename;
	var lines = new List<string>();
	string s = "";
 	using (StreamReader sr = File.OpenText(fullPath)) {
		while ((s = sr.ReadLine()) != null) {
			var line = s.Trim();
			if (!String.IsNullOrWhiteSpace(s))
				lines.Add(s.Trim());
		}
	}
	return lines;
}

public List<string> GetListFromFileOrElse(string filename, Func<List<string>> fallback, bool writeToFileIfMissing = true) {
	var result = new List<string>();
	if (String.IsNullOrWhiteSpace(BasePath)) {
		throw new Exception("Set BasePath to the desired location for your source data and requests.");
	}
	if (string.IsNullOrWhiteSpace(filename)) {
		throw new Exception("Parameter 'filename' cannot be null");
	}
	if (File.Exists(BasePath + filename)) {
		string.Format("'{0}' already exists, reading from file.", filename).Dump();
		result = ReadTrimmedLinesFromFile(filename);
	} else {
		string.Format("'{0}' does not exist, performing fallback action.", filename).Dump();
		result = fallback();
		
		if (writeToFileIfMissing) {
			string.Format("About to create '{0}' using results of fallback action.", filename).Dump();
			WriteLinesToFile(filename, result);
		}
	}
	return result;
}