#!/bin/sh -x

echo "Installing Scala"
cd /usr/local/
wget http://scala-lang.org/files/archive/scala-2.11.0.tgz
tar zxvf scala-2.11.0.tgz
rm scala-2.11.0.tgz
mv /usr/local/scala-2.11.0 /usr/local/scala

echo 'PATH=$PATH:/usr/local/scala/bin'  >> /etc/profile
echo 'export PATH'  >> /etc/profile