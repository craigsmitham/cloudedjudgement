#!/bin/sh -x

echo "Installing the Gradle"
cd /usr/local/

wget https://services.gradle.org/distributions/gradle-1.11-all.zip
unzip gradle-1.11-all.zip
rm gradle-1.11-all.zip
mv /usr/local/gradle-1.11 /usr/local/gradle

echo 'PATH=$PATH:/usr/local/gradle'  >> /etc/profile
echo 'export PATH'  >> /etc/profile
