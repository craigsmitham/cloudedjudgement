#!/bin/sh -x

echo "Installing the Play Framework"
cd /usr/local/

wget http://downloads.typesafe.com/play/2.2.2/play-2.2.2.zip
unzip play-2.2.2.zip
rm play-2.2.2.zip
mv /usr/local/play-2.2.2 /usr/local/play

echo 'PATH=$PATH:/usr/local/play'  >> /etc/profile
echo 'export PATH'  >> /etc/profile
