#!/bin/sh -x
# https://rtcamp.com/tutorials/nodejs/node-js-npm-install-ubuntu/
echo "Installing node"

curl http://nodejs.org/dist/latest/node-v0.10.26-linux-x64.tar.gz | tar zx --strip=1 -C /usr/local
