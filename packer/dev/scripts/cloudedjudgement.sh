#!/bin/sh


mkdir /clouded-judgement
mkdir /clouded-judgement/data

chown -R vagrant /clouded-judgement

echo 'CLOUD_ENV=AWS'  >> ~/.profile
echo 'export CLOUD_ENV'  >> /.profile