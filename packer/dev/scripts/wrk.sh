#!/bin/sh -x

echo "Installing wrk"
cd ~/
mkdir -p tools
cd tools
git clone https://github.com/wg/wrk.git
cd wrk
make

ln -s  ~/tools/wrk/wrk ~/bin/wrk
