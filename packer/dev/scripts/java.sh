#!/bin/sh -x
echo "Installing Java 8"

cd ~/
wget --no-check-certificate --no-cookies - --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8-b132/jdk-8-linux-x64.tar.gz

mkdir -p /usr/lib/jvm
mv jdk-8-linux-x64.tar.gz /usr/lib/jvm
cd /usr/lib/jvm
tar -zxvf jdk-8-linux-x64.tar.gz
rm jdk-8-linux-x64.tar.gz
update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0/bin/javac" 1
update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0/bin/java" 1
update-alternatives --set "javac" "/usr/lib/jvm/jdk1.8.0/bin/javac"
update-alternatives --set "java" "/usr/lib/jvm/jdk1.8.0/bin/java"

echo 'JAVA_HOME=/usr/lib/jvm/jdk1.8.0'  >> /etc/profile
echo 'PATH=$PATH:$JAVA_HOME/bin'  >> /etc/profile
echo 'export JAVA_HOME'  >> /etc/profile
echo 'export PATH'  >> /etc/profile
