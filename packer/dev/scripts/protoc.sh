#!/bin/sh -x

echo "Installing Protocol Buffers"
cd ~/
wget https://protobuf.googlecode.com/files/protobuf-2.5.0.tar.gz
tar zxvf protobuf-2.5.0.tar.gz
rm protobuf-2.5.0.tar.gz
cd protobuf-2.5.0

./configure
make
make check
make install

echo 'LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib' >> /etc/profile
echo 'export LD_LIBRARY_PATH'  >> /etc/profile