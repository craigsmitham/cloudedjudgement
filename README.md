# Welcome #

This repository contains the work of team _Clouded Judgement_ (Cameron Buescher and Craig Smitham) 
for Pariveda Solutions's 2014 Cloud Challenge. We've included complete source code of the final solution
as well as additional research and reference implementations. 

## System Requirements ##

* Java 8 JDK
* Google Protocol Buffers installed and accessible via your PATH system environment variable
* IDE/editor of your choice

## Quick Start ##

1. To get started, clone this repository:

	`git clone https://bitbucket.org/craigsmitham/cloudedjudgement`
	
2. Navigate to the jvm project folder

	`cd cloudedjudgment/jvm`
	
3. Build the project

	`gradlew build`
	

## Solution Orientation ##

### Source Code Organization ###

The primary solution source code lives in the `jvm` directory. The gradle wrapper script (see below) 
lives in this directory, and most build commands should be executed from here.

### Gradle ###

[Gradle](http://www.gradle.org/) is used to automate builds, testing, and deployments. 
A Gradle wrapper script (gradlew.sh or gradlew.bat) is included in the `jvm` source directory. 
Whenever Gradle tasks are executed, use `gradlew` command instead of `gradle`. On first use, `gradlew` 
will download and install a distribution of Gradle exclusively for this project. Subsequent 
tasks executed via the `gradlew` wrapper will use this unique installation of Gradle.


## Development ##

All that is needed to contribute is the ability to run Gradle tasks and a text editor. You may use Sublime Text, Vim, or an IDE such as Eclipse or Intellij IDEA.  

Setting up Intellij or Eclipse will create special project files that are ignored by git and only present on your development machine. If want clean your cloned repository of any of these files and start fresh, run `git clean -xdfn` to preview a git clean operation, then `git clean -xdf` to purge your working directory of any uncommitted or ignored files, including your IDE project files. 

#### Intellij IDEA Set Up ####
[Intellij IDEA](http://www.jetbrains.com/idea/) (including the free Community Edition) has first class support for Gradle projects.

1. From the welcome screen, select 'Import Project'.
2. Select the `jvm` source directory from the directory prompt. 
3. When prompted to import from existing sources or from an external model, choose the Gradle option under external model.
4. Click 'Finish' to accept the default settings
5. The Gradle tasks tool window may be opened by seleting View > Tool Windows > Gradle


#### Eclipse Set Up ####
If you wish to use Eclipse, Gradle tooling is recommended. 

1. Install [Eclipse Gradle integration](https://github.com/spring-projects/eclipse-integration-gradle/) or simply install the [Spring Tool Suite](http://spring.io/tools) distribution of Eclipse.
2. In Eclipse, click File > Import then select Gradle Project
3. In the Import Gradle Project wizard, set the root folder to the the `jvm` source directory
4. Click 'Build Model'
5. Select all projects
6. Click 'Finish'
7. Enable the Gradle Tasks navigating to Window > Show View > Other and selecting Gradle Tasks
8. Learn more about the Gradle/Eclispe integration at [Eclipse-Integration-Gradle wiki](https://github.com/spring-projects/eclipse-integration-gradle/wiki).

## Testing ##
TODO

## Running Locally ##

TODO

Execute the 'jettyRun' Gradle task under the web or webapi project to run either of the web projects.

## Deploying to Azure ##

## Deploying to AWS ##

## Running the Test Rig ##
### Azure ###
### AWS ###

## System Architecture ##

The solution is composed of an e-commerce website and a tcp web api 
that are powered by a product and order service modules. Additional components include
a client library and test rig to facilitate performance testing.

### Cloud Infrastructure: AWS and Azure Services ###
The compute and storage architecture is laregly the same on both Azure and AWS:

* Compute Services: [Azure IaaS Linux VMs](http://azure.microsoft.com/en-us/services/virtual-machines/) and [Amazon EC2](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html)
* Order Service Storage: [Amazon EBS](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEBS.html) and [Azure Storage Disks](http://azure.microsoft.com/en-us/services/storage/) 
* Product Service Storage:  TODO; Cameron

### Metrics, Monitoring, and Instrumentation
#### Approach
TODO
#### Tools & Resources
* 5 things you didn't know about ... Java performance monitoring ([Part 1](http://www.ibm.com/developerworks/java/library/j-5things7/index.html), [Part 2](http://www.ibm.com/developerworks/java/library/j-5things7/index.html))
* [Metrics](http://metrics.codahale.com/) by Coda Hale

### Order Service ###
#### Capabilities ####
The order service supports the creation of customer orders, adding items to orders, and submitting orders.
#### Requirements ####
* **[SCORED]** 30,000 “add item” requests per minute (500+/second)
	* Measured as time taken from request until the data is in the backing store
	* “OK” Response to the user must come within 500 milliseconds of request
	* All orders must be persisted to data store within 10 seconds of submission
* **[SCORED]**3,000 “submit order” requests per minute (50+/second)
	* Orders must contain an average of 10 items per order and varying SKUs
	* Measured as time taken from request until the data is in the backing store
	* “OK” Response to the user must come within 500 milliseconds of request
	* All orders must be persisted to data store within 10 seconds of submission
* [TESTING] Order service should be able to retrieve order state


#### Design ####
To maximally contribute to overall solution scoring, the order service is optimized to provide minimum latency and maximum throughput of command operations (add item, submit order) over query operations (retraining the state of an order). 

The order service uses [Event Sourcing](http://martinfowler.com/eaaDev/EventSourcing.html), an architectural approach that allows for rich domain modeling, but also very scalable architectures. The event log is the canonical representation of the application state for all orders in the system. An order is created when a  _CreateOrder_ event occurs, and it is modified when a related _AddItemToOrder_ or _SubmitOrder_ occurs. 

Because the system is small and has limited requirements, there are a few additional trade-offs that we make for the sake of overall performance and simplicity. In a real event-driven system, it often makes sense to distinguish between the incoming command messages (_AddItemToOrder_) and the events that indicate a command has been processed (_ItemAddedToOrder_), usually persisting the incoming command log. We are not making this distinction , simply labeling our incoming messages as events. 

#### Implementation ####
The order service is exposed through a TCP/IP web service, and consumed by a TCP/IP client library. Internally, the incoming messages are handed off to an [LMAX Disruptor](http://lmax-exchange.github.io/disruptor/), a high performance inter-thread messaging library, that in turn has an event listener logging the messages to persistent storage using [Java Chronicle](https://github.com/OpenHFT/Java-Chronicle), a low latency library for persisting messages. For our requirements, we're done with event processing. Additional event handlers may be implemented to handle additional business logic. Reading an order is managed by replaying the event log and reconstituting an order state base on its event stream. 

#### Testing 
##### Testing Add Items To Order
* Test Rig Parameters and Options
	* _-t addItems_
	* _-c_, clear event log (default: true, omit for false)
	*  _-o_, number of orders to create (default: 1K)
	* _-i_, number of items per order (default: 10)
	* _-d_, test duration, in seconds (default: 10)
	* _-l_, operations per second rate limit (default: 2, omit for none)
* Steps:
	* Clear event log 
	* Create _o_ orders
	* For _t_ seconds, add _i_ items to each order at a limit _l_ operations per second
* Test Report
	* Client: # of orders created, # of items added, start, stop, test duration, effective operations per second
	* Server: # of orders created, # of items added, start, stop, timespan, effective operations per second, time from handled to persistance?
##### Testing submit order
* Run add items to order test

### Product Service ###
#### Design ####
#### Implementation ####



### Website ###
#### Design ####
#### Implementation ####

### Client ###
#### Design ####
#### Implementation ####

### Test Rig ###
#### Design ####
#### Implementation ####