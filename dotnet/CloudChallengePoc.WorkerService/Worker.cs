﻿using System;
using CloudChallengePoc.Msmq;
using CloudChallengePoc.ZeroMq;

namespace CloudChallengePoc.Worker
{
    class Worker
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Worker Service");
            Console.WriteLine("Press ENTER to start.");
            Console.ReadLine();
            Console.WriteLine("Preparing to receive messages");
            using (IReciever reciever = new ZeroMqReceiver())
            {
                reciever.Run();
            }
            Console.WriteLine("Completed, press ENTER to exit");
            Console.ReadLine();
        }
    }
}
