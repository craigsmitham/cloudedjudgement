﻿using System;
using CloudChallengePoc.Msmq;
using CloudChallengePoc.ZeroMq;

namespace CloudChallengePoc.TestHost
{
    class TestHost
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Test Host");
            Console.WriteLine("Ready to publish messages");
            
            using (IPublisher publisher = new ZeroMqPublisher())
            {
                publisher.Run();
            }
            Console.ReadLine();
        }
    }
}
