﻿using System.Messaging;

namespace CloudChallengePoc.Msmq
{
    internal static class MsmqHelper
    {
        private const string QueuePath = @".\Private$\SomeTestName";
        public static MessageQueue GetOrCreateQueue()
        {
            if (!MessageQueue.Exists(QueuePath))
                MessageQueue.Create(QueuePath);

            var mq = new MessageQueue(QueuePath)
            {
                Formatter = new BinaryMessageFormatter(),
            };

            mq.Purge();

            return mq;
        }
    }
}
