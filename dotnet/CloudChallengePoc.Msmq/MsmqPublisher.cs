﻿using System;
using System.Messaging;

namespace CloudChallengePoc.Msmq
{
    public class MsmqPublisher : PublisherBase<TestMessage>
    {
        private readonly MessageQueue _msmq;

        public MsmqPublisher()
        {
            _msmq = MsmqHelper.GetOrCreateQueue();
            _msmq.Purge();
        }

        public override Func<int, TestMessage> MessageGenerator
        {
            get
            {
                return i => new TestMessage
                {
                    Id = +i,
                    Body = Guid.NewGuid().ToString()
                };
            }
        }

        public override void SendMessage(TestMessage message)
        {
            _msmq.Send(message);
        }

        public override void Dispose()
        {
            _msmq.Dispose();
        }
    }

}