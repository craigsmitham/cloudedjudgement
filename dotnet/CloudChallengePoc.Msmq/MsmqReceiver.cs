﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Messaging;

namespace CloudChallengePoc.Msmq
{
    public class MsmqReceiver : ReceiverBase
    {
        private MessageQueue _mq;
        public override void Run()
        {
            _mq = MsmqHelper.GetOrCreateQueue();
            var stopwatch = new Stopwatch();
            var handled = 0;
            var receive = true;
            var eventStore = new List<TestMessage>();
            //var message = _mq.Receive(TimeSpan.Zero);
            //var test = (((TestMessage)message.Body));
            stopwatch.Start();
            //var messages = mq.GetAllMessages();
            //eventStore.AddRange(messages.Select(m => ((TestMessage)m.Body)).ToList());
            while (receive)
            {
                try
                {
                    var msg = _mq.Receive(TimeSpan.Zero);
                }
                catch (MessageQueueException e)
                {
                    receive = false;
                }
            }
            stopwatch.Stop();
            PrintStatus(eventStore.Count, stopwatch.Elapsed.TotalMilliseconds);
        }

        public override void Dispose()
        {
            _mq.Dispose();
        }
    }
}