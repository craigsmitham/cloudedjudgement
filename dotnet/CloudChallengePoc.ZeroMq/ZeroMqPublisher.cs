﻿using System;
using System.Text;
using ZeroMQ;

namespace CloudChallengePoc.ZeroMq
{
    public class ZeroMqPublisher : PublisherBase<string>
    {
        private readonly ZmqContext _context;
        private readonly ZmqSocket _publisher;
        public ZeroMqPublisher()
        {
            _context = ZmqContext.Create();
            _publisher = _context.CreateSocket(SocketType.PUSH);
            _publisher.Bind("tcp://127.0.0.1:5555");
        }

        public override Func<int, string> MessageGenerator
        {
            get { return i => i.ToString(); }
        }

        public override void SendMessage(string message)
        {
            _publisher.Send(message, Encoding.UTF8);
        }

        public override void Dispose()
        {
            _context.Dispose();
            _publisher.Dispose();
        }
    }
}