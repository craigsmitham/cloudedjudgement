using System;
using System.Diagnostics;
using System.Text;
using ZeroMQ;

namespace CloudChallengePoc.ZeroMq
{
    public class ZeroMqReceiver : ReceiverBase
    {
        private readonly ZmqContext _context;
        private readonly ZmqSocket _subscriber;
        private TimeSpan _receiveTimeout = TimeSpan.FromSeconds(5);

        public ZeroMqReceiver()
        {
            _context = ZmqContext.Create();
            _subscriber = _context.CreateSocket(SocketType.PULL);

            _subscriber.Connect("tcp://127.0.0.1:5555");
            //_subscriber.SubscribeAll();
        }

        public override void Run()
        {
            var count = 0;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var receive = true;
            while (receive)
            {
                var message = _subscriber.Receive(Encoding.UTF8, _receiveTimeout);
                if (message != null)
                    count++;
                else
                {
                    stopwatch.Stop();
                    receive = false;
                }
            }
            var elapsedMilliseconds = stopwatch.ElapsedMilliseconds - _receiveTimeout.TotalMilliseconds;
            PrintStatus(count, elapsedMilliseconds);
        }

        public override void Dispose()
        {
            _context.Dispose();
            _subscriber.Dispose();
        }
    }
}