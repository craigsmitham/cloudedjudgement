﻿using System;

namespace CloudChallengePoc
{
    public interface IReciever : IDisposable
    {
        void Run();
    }
}