﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CloudChallengePoc
{
    public abstract class PublisherBase<TMessage> : IPublisher
    {
        public void Run()
        {
            var thousandMultiplier = 60;
            while (thousandMultiplier > 0)
            {
                Console.WriteLine("How may messages would you like to send (thousands)? [{0} default, 0 to quit]",
                    thousandMultiplier);
                var thousandMultiplierInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(thousandMultiplierInput))
                    thousandMultiplier = int.Parse(thousandMultiplierInput);
                var messages = Enumerable.Range(0, 1000 * thousandMultiplier)
                    .Select(MessageGenerator).ToList();
                var timer = new Stopwatch();
                timer.Start();
                var responseTimeLog = new List<decimal>();
                var responseTimer = new Stopwatch();
                foreach (var msg in messages)
                {
                    responseTimer.Start();
                    SendMessage(msg);
                    responseTimeLog.Add(responseTimer.ElapsedMilliseconds);
                    responseTimer.Restart();
                }
                timer.Stop();
                Console.WriteLine("Sent {0} messages in {1}ms", messages.Count, timer.ElapsedMilliseconds);
                var maxResponseTime = responseTimeLog.Max();
                var averageResponseTime = responseTimeLog.Average();
                Console.WriteLine("Average response time: {0}, max response time: {1}", averageResponseTime, maxResponseTime);
                timer.Reset();
            }
        }

        public abstract Func<int, TMessage> MessageGenerator { get; }
        public abstract void SendMessage(TMessage message);
        public abstract void Dispose();
    }
}