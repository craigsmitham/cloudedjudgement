﻿using System;

namespace CloudChallengePoc
{
    public interface IPublisher : IDisposable
    {
        void Run();
    }
}