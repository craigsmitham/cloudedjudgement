using System;

namespace CloudChallengePoc
{

    [Serializable]
    public class TestMessage
    {
        public int Id { get; set; }
        public string Body { get; set; }
    }
}