﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudChallengePoc.Domain
{
    public interface IHandle<in TCommand>
    {
        void Handle(TCommand command);
    }

    public interface IEventStore
    {
        void Log(object @event);
        void Log(List<object> events);
    }

    public class DeferredEventStoreFacade : IEventStore
    {
        public void Log(object @event)
        {
            throw new NotImplementedException();
        }

        public void Log(List<object> events)
        {
            throw new NotImplementedException();
        }
    }

    public class BufferedEventStore : IEventStore
    {
        public void Log(object @event)
        {
            throw new NotImplementedException();
        }

        public void Log(List<object> events)
        {
            throw new NotImplementedException();
        }
    }


    // Commands
    public class SubmitOrder { }
    public class AddItemToOrder { }

    // Events
    public class OrderSubmitted { }
    public class ItemAddedToOrder { }

    public class OrderService
        : IHandle<SubmitOrder>,
        IHandle<AddItemToOrder>
    {
        private readonly IEventStore _eventStore;

        public OrderService(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public void Handle(SubmitOrder command)
        {
            // Submit order is idempotent, go ahead and log the event
            // This is where command validation would occur
            var @event = new OrderSubmitted();
            _eventStore.Log(@event);
        }

        public void Handle(AddItemToOrder command)
        {
            var @event = new ItemAddedToOrder();
            _eventStore.Log(@event);
        }
    }
}
