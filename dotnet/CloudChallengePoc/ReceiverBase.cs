﻿using System;

namespace CloudChallengePoc
{
    public abstract class ReceiverBase : IReciever
    {
        public abstract void Run();
        public void PrintStatus(int messageCount, double totalMilliseconds)
        {
            var messagesPerSecond =
                1000 * (messageCount / totalMilliseconds);
            Console.WriteLine("Handled {0} messges in {1}ms ({2} msg/sec)", messageCount, totalMilliseconds, messagesPerSecond);
        }

        public abstract void Dispose();
    }
}