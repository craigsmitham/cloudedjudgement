package com.cloudedjudgement.services.orderService;

import com.cloudedjudgement.shared.messages.ProtoMessages;
import com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import net.openhft.lang.io.Bytes;
import net.openhft.lang.io.serialization.BytesMarshallable;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;


public class Order implements Serializable {
    private String orderId;
    private Map<String, Integer> itemStore = new HashMap<>();
    private Integer timesSubmitted;
    private static final long serialVersionUID = 1;


    public Order() {
    }

    public Order(byte[] protobufArray) {
        try {
            ProtoMessages.OrderEntity entity = ProtoMessages.OrderEntity.parseFrom(protobufArray);
            timesSubmitted = entity.getTimesSubmitted();
            orderId = entity.getOrderId();
            entity.getItemsList().forEach(iq -> {
               itemStore.put(iq.getItemId(), iq.getQuantity());
            });
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }

    public Order(String orderId) {
        this.orderId = orderId;
    }


    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }



    public void Handle(OrderMessage command) {
        switch (command.getType()) {
            case ADD_ITEM_TO_ORDER:
                addItemToOrder(command.getItemId());
                break;
            case CREATE_ORDER:
                orderId = command.getOrderId();
                break;
            case SUBMIT_ORDER:
                submitOrder();
                break;
            default:
                break;
        }
    }

    public void submitOrder() {
        if (timesSubmitted == null)
            timesSubmitted = 0;
        timesSubmitted++;
    }
    public void addItemToOrder(String itemId) {
        itemStore.put(itemId, itemStore.containsKey(itemId) ? itemStore.get(itemId) + 1 : 1);
    }

    public Integer getTimesSubmitted() {
        return timesSubmitted;
    }

    public List<OrderItemSummary> getItems() {
        List<OrderItemSummary> items = new ArrayList<>();
        itemStore.entrySet().forEach(iq -> items.add(new OrderItemSummary(iq.getKey(), iq.getValue())));
        return items;
    }

    public String getOrderId() {
        return orderId;
    }

    public byte[] toProtobufByteArray() {
        if (timesSubmitted == null)
            timesSubmitted = 0;
        ProtoMessages.OrderEntity.Builder builder = ProtoMessages.OrderEntity.newBuilder()
                .setOrderId(orderId)
                .setTimesSubmitted(timesSubmitted);
        itemStore.forEach((itemId, quantity) -> {
            builder.addItems(ProtoMessages.ItemQuantityEntity.newBuilder().setItemId(itemId).setQuantity(quantity));
        });
        return builder.build().toByteArray();
    }



}

