package com.cloudedjudgement.services.orderService;

import java.util.Set;
import java.util.UUID;

public interface OrderService {
    void createOrder();

    Order getOrder(String orderId);

    void addItemToOrder(String orderId, String itemId);

    void submitOrder(String orderId);

    Set<String> getOrderIds();

    void createOrder(String orderId);
}
