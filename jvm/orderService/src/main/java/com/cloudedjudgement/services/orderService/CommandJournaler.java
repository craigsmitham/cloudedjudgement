package com.cloudedjudgement.services.orderService;

import com.cloudedjudgement.shared.Journal;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;
import com.lmax.disruptor.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.codahale.metrics.MetricRegistry.name;

public class CommandJournaler implements EventHandler<OrderMessageEnvelope> {
    Journal commandJournal;
    MetricRegistry metrics;
    Histogram timeTillPersistedMs;
    Logger logger = LoggerFactory.getLogger(CommandJournaler.class);

    public CommandJournaler(Journal commandJournal, MetricRegistry metrics) {
        this.commandJournal = commandJournal;
        this.metrics = metrics;
        this.timeTillPersistedMs = metrics.histogram(name(Journal.class, "commands", "timeTillPersistedMs"));
    }

    @Override
    public void onEvent(OrderMessageEnvelope event, long sequence, boolean endOfBatch) throws Exception {
        commandJournal.log(event.getMessageBytes());
        timeTillPersistedMs.update((System.nanoTime() - event.getNanoTimeReceived()) / 1000000);
    }
}

