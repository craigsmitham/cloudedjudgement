package com.cloudedjudgement.services.orderService;

import net.openhft.chronicle.IndexedChronicle;

import java.io.FileNotFoundException;

public final class EventStoreFactory {
    public static IndexedChronicle getChronicle() throws FileNotFoundException {
        String basePath = System.getProperty("java.io.tmpdir") + "OrderServiceEventLog";
        IndexedChronicle chronicle = new IndexedChronicle(basePath);
        return chronicle;
    }
}
