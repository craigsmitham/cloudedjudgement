package com.cloudedjudgement.services.orderService;

import com.lmax.disruptor.RingBuffer;

public class OrderServiceMessageProducer {
    private final RingBuffer<OrderMessageEnvelope> ringBuffer;

    public OrderServiceMessageProducer(RingBuffer<OrderMessageEnvelope> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void onReceive(byte[] messageBytes) {
        long sequence = ringBuffer.next();
        try {
            OrderMessageEnvelope event = ringBuffer.get(sequence);
            event.setMessageBytes(messageBytes);
            event.setNanoTimeReceived(System.nanoTime());
        } finally {
            ringBuffer.publish(sequence);
        }
    }
}
