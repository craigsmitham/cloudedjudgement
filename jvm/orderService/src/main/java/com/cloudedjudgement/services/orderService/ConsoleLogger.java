package com.cloudedjudgement.services.orderService;

import com.lmax.disruptor.EventHandler;

import static com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage;

public class ConsoleLogger implements EventHandler<OrderMessageEnvelope> {

    @Override
    public void onEvent(OrderMessageEnvelope event, long sequence, boolean endOfBatch) throws Exception {
        OrderMessage message = OrderMessage.parseFrom(event.getMessageBytes());
        System.out.println("SIZE: " + message.getSerializedSize());
        System.out.println("TYPE: " + message.getType());
        System.out.println("OrderId: " + message.getOrderId());
        System.out.println("ItemId: " + message.getItemId());
    }
}
