package com.cloudedjudgement.services.orderService;

import com.cloudedjudgement.shared.CloudedJudgementContext;
import com.cloudedjudgement.shared.Journal;
import com.codahale.metrics.Counter;

import java.util.*;
import java.util.function.Consumer;

import static com.cloudedjudgement.shared.messages.OrderMessages.AddItem;
import static com.cloudedjudgement.shared.messages.OrderMessages.CreateOrder;
import static com.cloudedjudgement.shared.messages.OrderMessages.SubmitOrder;
import static com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage;
import static com.codahale.metrics.MetricRegistry.name;

public class CommandSourcedOrderService implements OrderService {
    private Journal commandJournal;
    private Counter addItemToOrder;
    private Counter createOrder;
    private Counter submitOrder;
    private CloudedJudgementContext context;

    public CommandSourcedOrderService(Journal commandJournal, CloudedJudgementContext context) {
        this.commandJournal = commandJournal;
        this.context = context;
        addItemToOrder = context.getMetrics().counter(name(CommandSourcedOrderService.class, "addItemItemToOrder"));
        createOrder = context.getMetrics().counter(name(CommandSourcedOrderService.class, "createOrder"));
        submitOrder = context.getMetrics().counter(name(CommandSourcedOrderService.class, "submitOrder"));
    }

    public static CommandSourcedOrderService create(CloudedJudgementContext context) {
        return new CommandSourcedOrderService(new Journal("OrderServiceCommands"), context);
    }

    @Override
    public void createOrder(String orderId) {
        logCommand(CreateOrder(orderId));
        createOrder.inc();
    }

    @Override
    public void createOrder() {
        logCommand(CreateOrder());
        createOrder.inc();
    }

    @Override
    public void submitOrder(String orderId) {
        logCommand(SubmitOrder(orderId));
        submitOrder.inc();
    }

    @Override
    public void addItemToOrder(String orderId, String itemId) {
        logCommand(AddItem(orderId, itemId));
        addItemToOrder.inc();
    }

    @Override
    public Order getOrder(String orderId) {
        Order order = new Order();
        consumeCommands(command -> {
            if (orderId.compareTo(command.getOrderId()) == 0) {
                order.Handle(command);
            }
        });
        return order.getOrderId() != null ? order : null;
    }

    @Override
    public Set<String> getOrderIds() {
        Set<String> orderIds = new HashSet<>();
        consumeCommands(command -> orderIds.add(command.getOrderId()));
        return orderIds;
    }

    private void consumeCommands(Consumer<OrderMessage> orderMessageConsumer) {
        commandJournal.consume(orderMessageConsumer);
    }

    private void logCommand(OrderMessage message) {
        commandJournal.log(message.toByteArray());
    }

}
