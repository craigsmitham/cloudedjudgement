package com.cloudedjudgement.services.orderService;

import java.util.UUID;

public class OrderItemSummary {
    private String itemId;
    private Integer quantity;

    public OrderItemSummary(String itemId, Integer quantity) {
        this.itemId = itemId;
        this.quantity = quantity;
    }

    public String getItemId() {
        return itemId;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
