package com.cloudedjudgement.services.orderService;

public class OrderMessageEnvelope {
    private byte[] messageBytes;
    private long nanoTimeReceived;

    public byte[] getMessageBytes() {
        return messageBytes;
    }

    public void setMessageBytes(byte[] messageBytes) {
        this.messageBytes = messageBytes;
    }

    public long getNanoTimeReceived() {
        return nanoTimeReceived;
    }

    public void setNanoTimeReceived(long nanoTimeReceived) {
        this.nanoTimeReceived = nanoTimeReceived;
    }
}
