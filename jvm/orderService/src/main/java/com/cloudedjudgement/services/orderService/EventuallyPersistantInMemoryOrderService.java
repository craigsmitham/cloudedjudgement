package com.cloudedjudgement.services.orderService;

import com.cloudedjudgement.shared.CloudedJudgementContext;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Histogram;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import static com.codahale.metrics.MetricRegistry.name;

public class EventuallyPersistantInMemoryOrderService implements OrderService {
    private ConcurrentHashMap<String, Order> inMemoryOrders;
    private Set<String> createdOrderIds = new HashSet<>();
    private Counter addItemToOrder;
    private Counter createOrder;
    private Counter submitOrder;
    private Histogram timeToPersistOrders;
    private String ordersFilePath = "/clouded-judgement/data/orders";

    private final CloudedJudgementContext context;
    static final Logger logger = LoggerFactory.getLogger(EventuallyPersistantInMemoryOrderService.class);

    public static EventuallyPersistantInMemoryOrderService createAndSchedulePersistence(CloudedJudgementContext context) {
        EventuallyPersistantInMemoryOrderService orderService = create(context);
        orderService.schedulePersistance();
        return orderService;
    }

    public static EventuallyPersistantInMemoryOrderService create(CloudedJudgementContext context) {
        EventuallyPersistantInMemoryOrderService orderService = new EventuallyPersistantInMemoryOrderService(context);
        orderService.restoreOrders();
        return orderService;
    }

    public void schedulePersistance() {
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            logger.info("Saving orders");
            saveOrders();
        },10, 8, TimeUnit.SECONDS);
    }

    private EventuallyPersistantInMemoryOrderService(CloudedJudgementContext context) {
        this.context = context;
        addItemToOrder = context.getMetrics().counter(name(EventuallyPersistantInMemoryOrderService.class, "addItemItemToOrder"));
        createOrder = context.getMetrics().counter(name(EventuallyPersistantInMemoryOrderService.class, "createOrder"));
        submitOrder = context.getMetrics().counter(name(EventuallyPersistantInMemoryOrderService.class, "submitOrder"));
        timeToPersistOrders = context.getMetrics().histogram(name(EventuallyPersistantInMemoryOrderService.class, "timeToPersistOrders"));
    }

    public void restoreOrders() {
        File ordersFile = new File(ordersFilePath);
        if (ordersFile.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(ordersFile);
                inMemoryOrders = SerializationUtils.deserialize(fileInputStream);
                fileInputStream.close();
            } catch (FileNotFoundException e) {
                logger.error("Cannot find file {}", ordersFilePath);
                throw new RuntimeException(e);
            } catch (IOException e) {
                logger.error("IO exception decentralizing file {}", ordersFilePath);
                throw new RuntimeException(e);
            }
        }
        else {
            try {
                ordersFile.createNewFile();
                inMemoryOrders = new ConcurrentHashMap<>();
            } catch (IOException e) {
                logger.error("Error creating {}", ordersFilePath);
                throw new RuntimeException(e);
            }
        }
    }

    private void saveOrders() {
        File ordersFile = new File(ordersFilePath);
        if (ordersFile.exists())
            ordersFile.delete();

        try {

            long startTime = System.nanoTime();
            FileOutputStream fileOutputStream = new FileOutputStream(ordersFile);
            SerializationUtils.serialize(inMemoryOrders, fileOutputStream);
            fileOutputStream.close();
            timeToPersistOrders.update((System.nanoTime() - startTime)/1000000);
        } catch (FileNotFoundException e) {
            logger.error("Cannot find file {}", ordersFilePath);
            throw new RuntimeException(e);
        } catch (IOException e) {
            logger.error("Exception saving orders", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createOrder() {
        String orderId = UUID.randomUUID().toString();
        createOrder(orderId);
    }

    @Override
    public void createOrder(String orderId) {
        Order order = new Order(orderId);
        //Order setOp = inMemoryOrders.put(orderId, order);
        createdOrderIds.add(orderId);
        updateOrder(order);
        createOrder.inc();
    }

    @Override
    public Order getOrder(String orderId) {
        Order order = retrieveOrder(orderId);
        // not sure why I have to do this:
        //order.setOrderId(orderId);
        return order;
    }

    @Override
    public void addItemToOrder(String orderId, String itemId) {
        Order order = retrieveOrder(orderId);
        order.addItemToOrder(itemId);
        updateOrder(order);
        addItemToOrder.inc();
    }

    @Override
    public void submitOrder(String orderId) {
        Order order = retrieveOrder(orderId);
        order.submitOrder();
        updateOrder(order);
        submitOrder.inc();
    }

    @Override
    public Set<String> getOrderIds() {
        return inMemoryOrders.keySet();
    }

    private void updateOrder(Order order) {
        inMemoryOrders.put(order.getOrderId(), order);
    }

    private Order retrieveOrder(String orderId) {
        return inMemoryOrders.get(orderId);
    }


}
