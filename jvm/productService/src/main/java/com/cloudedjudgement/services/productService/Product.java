package com.cloudedjudgement.services.productService;

public class Product {
	private String sku;
	private String name;
	private String toStringCache;
	
	public Product() { }
	
	public Product(String sku) {
		this(sku, "");
	}
	
	public Product(String sku, String name) {		
		setSku(sku);
		setName(name);
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku.toUpperCase();
		
		// Update the toStringCache
		this.setToStringCache();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		
		// Update the toStringCache
		this.setToStringCache();	
	}
	
	/**
	 * Save the toString into a cache variable so that we can avoid
	 * re-allocating and garbage collecting the memory every time toString is called
	 */
	private void setToStringCache() {
		// format as JSON
		this.toStringCache = String.format("{\"sku\":\"%s\",\"name\":\"%s\"}", getSku(), getName());
	}
	
	/**
	 * Uses cached toString representation.  Any time the state of the object changes,
	 * this state needs to be updated. Use setToStringCache to reset.
	 */
	@Override
	public String toString() {
		return this.toStringCache;
	}		
}
