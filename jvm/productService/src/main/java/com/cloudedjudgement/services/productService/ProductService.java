package com.cloudedjudgement.services.productService;

import java.util.ArrayList;
import java.util.HashMap;

import com.amazonaws.util.StringUtils;
import com.cloudedjudgement.shared.CloudedJudgementContext;


public class ProductService {
	// the delimiter to use to split the given query string (i.e. name or SKU)
	private static final String SEARCH_TERM_DELIMITER = " ";
	private static ProductService theInstance;
    private final CloudedJudgementContext context;
	
	// A map of search terms to the list of products matching that term. 
	// The keys will be words in the product names and their SKU's; each value
	// will be the list of products with that search term, giving constant-time
	// access to the list of products matching with a given word in their name
	// or a given SKU.
	private HashMap<String, ArrayList<Product>> theProductIndex;
	
	// Implemented as a singleton to avoid unnecessary garbage collection - use 
	// getInstance() to construct
	private ProductService(CloudedJudgementContext context) {
        this.context = context;
        // Load the products into an index for searching over
		loadProducts();
	}	
	
	public static ProductService getInstance(CloudedJudgementContext context) {
		if(theInstance == null) {
			theInstance = new ProductService(context);
		}
		return theInstance;
	}

	// Will split the parameter into a list of search terms and return a list of the results
	public ArrayList<Product> search(String nameOrSku) {
		ArrayList<Product> results = new ArrayList<Product>();
		if(!StringUtils.isNullOrEmpty(nameOrSku)) {
			String[] searchTerms = getSearchKeys(nameOrSku);
			for (String term : searchTerms) {
				if (theProductIndex.containsKey(term)) {
					results.addAll(theProductIndex.get(term));
				}
			}
		}
		
		return results;
	}
	
	public static String listToString(ArrayList<Product> list) {
		// clear the string builder.  Choosing to use a SB here to reduce
		// allocation and GC of a lot of small strings. It's a static class
		// variable so that it'll get created once and we'll reuse.
		
		StringBuilder stringBuilder = new StringBuilder("[");
		for (int i=0; i<list.size(); ++i) {
			// using getJson method here to allow the instance to cache the json
			// representation instead of creating a new string with toString every time
			stringBuilder.append(list.get(i));
			stringBuilder.append(",");
		}
		
		if(list.size() > 0) {
			// Remove trailing comma
			stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		}
		
		stringBuilder.append("]");
		return stringBuilder.toString();
	}

	// Build the product hashmap
	private void loadProducts() {
		Iterable<Product> products = getProducts();
		
		// Create a hashmap keyed on words in the product names, and SKU's, with each
		// value a list of pointers to the products matching those search terms.
		theProductIndex = new HashMap<String, ArrayList<Product>>();

		for (Product product : products) {
			String[] keys = getSearchKeys(product);
			for (String key : keys) {
				if(theProductIndex.containsKey(key)) {
					// add the product to the list of those with this key
					theProductIndex.get(key).add(product);
				} else {
					// create a new entry in the map for this key, initializing the list with
					// this product					
					ArrayList<Product> productsWithKey = new ArrayList<>();
					productsWithKey.add(product);
					theProductIndex.put(key, productsWithKey);
				}
			}
		}

	}

	private Iterable<Product> getProducts() {
		ProductRepository repository = new ProductRepository(context);
		return repository.getAll();
	}
	
	private static String[] getSearchKeys(String nameOrSku) {
		// Convert to lowercase to provide case-insensitive comparison
		return nameOrSku.toLowerCase().split(SEARCH_TERM_DELIMITER);
	}
	
	private static String[] getSearchKeys(Product product) {
		return getSearchKeys(product.getName() + SEARCH_TERM_DELIMITER + product.getSku());
	}

}
