package com.cloudedjudgement.services.productService;

import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDB;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.Attribute;
import com.amazonaws.services.simpledb.model.Item;
import com.amazonaws.services.simpledb.model.SelectRequest;
import com.amazonaws.services.simpledb.model.SelectResult;
import com.amazonaws.util.StringUtils;
import com.cloudedjudgement.shared.CloudedJudgementContext;
import com.cloudedjudgement.shared.Environment;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.table.CloudTableClient;
import com.microsoft.azure.storage.table.DynamicTableEntity;
import com.microsoft.azure.storage.table.EntityProperty;
import com.microsoft.azure.storage.table.TableQuery;
import com.microsoft.azure.storage.table.TableQuery.QueryComparisons;

public class ProductRepository {

    private final CloudedJudgementContext context;

    public ProductRepository(CloudedJudgementContext context) {
        this.context = context;
    }

    /**
     * Checks the current environment and returns all products.
     *
     * @return
     */
    public Iterable<Product> getAll() {
        Iterable<Product> products = null;
        Environment environment = context.getEnvironment();

        try {
            products = environment == Environment.Azure ? getAllFromAzure()
                    : environment == Environment.AWS ? getAllFromAWS() : null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return products;
    }

    /**
     * Get products from Azure
     *
     * @return
     * @throws URISyntaxException
     * @throws StorageException
     * @throws InvalidKeyException
     */
    private Iterable<Product> getAllFromAzure() throws InvalidKeyException, URISyntaxException, StorageException {
        CloudStorageAccount storageAccount = CloudStorageAccount.parse(context.getAzureConnectionString());
        CloudTableClient tableClient = storageAccount.createCloudTableClient();

        String filter = TableQuery.generateFilterCondition("PartitionKey",
                QueryComparisons.EQUAL,
                context.getAzurePartitionKey());

        TableQuery<DynamicTableEntity> query = TableQuery
                .from(DynamicTableEntity.class)
                .where(filter);

        Iterable<DynamicTableEntity> entities = tableClient.getTableReference("Products").execute(query);
        ArrayList<Product> products = new ArrayList<Product>();

        for (DynamicTableEntity product : entities) {
            Product p = buildProductFromAzure(product);
            products.add(p);
        }

        return products;
    }

    ;

    /**
     * Get products from AWS
     *
     * @return
     * @throws Exception
     */
    private Iterable<Product> getAllFromAWS() throws Exception {
        AWSCredentials creds = new BasicAWSCredentials(context.getAwsAccessKey(), context.getAwsSecretKey());
        AmazonSimpleDB db = new AmazonSimpleDBClient(creds);

        // point to the west data center (default is east)
        db.setEndpoint("https://sdb.us-west-2.amazonaws.com");

        // Get all the products
        SelectResult queryResult = null;
        ArrayList<Product> mappedProducts = new ArrayList<Product>();
        SelectRequest query = new SelectRequest("select * from Products");

        // Query until all pages of results have been returned
        do {
            // Set the continuation token if we're past the first page of results
            if (queryResult != null) {
                query = query.withNextToken(queryResult.getNextToken());
            }

            // run the query
            queryResult = db.select(query);

            // map to products
            for (Item product : queryResult.getItems()) {
                Product p = buildProductFromAWS(product);
                mappedProducts.add(p);
            }
        } while (!StringUtils.isNullOrEmpty(queryResult.getNextToken()));

        return mappedProducts;
    }

    /**
     * Map an Table Storage DynamicTableEntity to a Product
     *
     * @param p
     * @return
     */
    private Product buildProductFromAzure(DynamicTableEntity p) {
        HashMap<String, EntityProperty> properties = p.getProperties();
        String name = properties.get("name").getValueAsString();
        String sku = properties.get("sku").getValueAsString();
        return new Product(sku, name);
    }

    /**
     * Map a SimpleDB record (as a list of attributes) to a Product
     *
     */
    private Product buildProductFromAWS(Item simpleDBRecord) throws Exception {
        String sku = null;
        String name = null;
        for (Attribute attribute : simpleDBRecord.getAttributes()) {
            if (attribute.getName().equalsIgnoreCase("sku")) {
                sku = attribute.getValue();
            } else if (attribute.getName().equalsIgnoreCase("name")) {
                name = attribute.getValue();
            }
        }

        if (sku == null || name == null) {
            throw new Exception("Could not map name or sku from SimpleDB record");
        }

        return new Product(sku, name);
    }
}
