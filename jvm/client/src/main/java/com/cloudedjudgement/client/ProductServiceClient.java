package com.cloudedjudgement.client;

import org.slf4j.Logger;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Socket;

import com.cloudedjudgement.shared.*;
import com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage;

public class ProductServiceClient {
	private Logger _logger;
    private Context _context;
    protected Socket _client;

    public ProductServiceClient(Logger logger) {
    	_logger = logger;
        _context = ZMQ.context(1);
        _client = _context.socket(ZMQ.REQ);
        _client.setSndHWM(100000);
        String hostAddress = "tcp://127.0.0.1:5558";
        _client.connect(hostAddress);
        _logger.info("Product Service client for {} started.", hostAddress);
    }
    
    public void send(String query) {
        _client.send(query);
        String resp = _client.recvStr();
    }

	public void close() throws Exception {
		_client.close();
        _context.term();
	} 
}
