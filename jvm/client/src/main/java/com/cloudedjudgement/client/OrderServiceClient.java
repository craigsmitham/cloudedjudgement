package com.cloudedjudgement.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

import static com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage;

public class OrderServiceClient {
    private Logger _logger = LoggerFactory.getLogger(OrderServiceClient.class);
    private ZMQ.Context _context;
    protected ZMQ.Socket _client;

    public OrderServiceClient() {
        _context = ZMQ.context(1);
        _client = _context.socket(ZMQ.REQ);
        _client.setSndHWM(100000);
        String hostAddress = "tcp://127.0.0.1:5559";
        _client.connect(hostAddress);
        _logger.info("Order Service Client client for {} started.", hostAddress);
    }

    public void send(OrderMessage message) {
        _client.send(message.toByteArray(), 0);
        String resp= _client.recvStr();
    }

    public void clearEventLog() {
        // not implemented
    }
}
