package com.cloudedjudgement.shared.messages;

import java.util.UUID;

import static com.cloudedjudgement.shared.messages.ProtoMessages.*;
import static com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage.OrderMessageType.*;

public class OrderMessages {
    public static OrderMessage AddItem(String orderId, String itemId) {
        return OrderMessage.newBuilder()
                .setType(ADD_ITEM_TO_ORDER)
                .setOrderId(orderId)
                .setItemId(itemId)
                .build();
    }


    public static OrderMessage CreateOrder() {
        return CreateOrder(UUID.randomUUID().toString());
    }

    public static OrderMessage CreateOrder(String orderId) {
        return OrderMessage.newBuilder()
                .setType(CREATE_ORDER)
                .setOrderId(orderId)
                .build();
    }

    public static OrderMessage SubmitOrder(String orderId) {
        return OrderMessage.newBuilder()
                .setType(SUBMIT_ORDER)
                .setOrderId(orderId)
                .build();
    }

    /*public static OrderMessage WarmUp() {
        return OrderMessage.newBuilder()
                .setType(WARM_UP)
                .build();
    }*/
}
