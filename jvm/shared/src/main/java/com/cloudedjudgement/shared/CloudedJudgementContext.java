package com.cloudedjudgement.shared;


import com.codahale.metrics.MetricRegistry;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CloudedJudgementContext {
    private final Logger logger = LoggerFactory.getLogger(CloudedJudgementContext.class);
    private Environment environment;
    private String azureConnectionString;
    private String azureProductsTable;
    private String azurePartitionKey;
    private String awsAccessKey;
    private String awsSecretKey;
    private Integer undertowBufferSizeMultiplier;
    private Integer undertowIoThreadMultiplier;
    private Integer undertowWorkerThreads;
    private Integer undertowSocketBacklog;
    private MetricRegistry metrics;

    public CloudedJudgementContext() {
        this(ConfigFactory.load());
    }

    public CloudedJudgementContext(Config config) {
        // Validate configuration
        config.checkValid(ConfigFactory.defaultReference(), "clouded-judgement");

        setEnvironment();
        logger.info("Cloud Enviroment: {}", environment);

        azureConnectionString = config.getString("clouded-judgement.azure.connection-string");
        azureProductsTable = config.getString("clouded-judgement.azure.products-table");
        azurePartitionKey = config.getString("clouded-judgement.azure.partition-key");
        awsAccessKey = config.getString("clouded-judgement.aws.access-key");
        awsSecretKey = config.getString("clouded-judgement.aws.secret-key");
        undertowBufferSizeMultiplier = config.getInt("clouded-judgement.undertow.buffer-size-multiplier");
        undertowIoThreadMultiplier = config.getInt("clouded-judgement.undertow.io-thread-multiplier");
        undertowWorkerThreads = config.getInt("clouded-judgement.undertow.worker-threads");
        undertowSocketBacklog = config.getInt("clouded-judgement.undertow.socket-backlog");

        metrics = new MetricRegistry();
    }

    private void setEnvironment() {
        // Set environtment
        String cloudEnvironment = System.getenv("CLOUD_ENV");
        RuntimeException environmentConfigurationException = new RuntimeException("The environment variable 'CLOUD_ENV' is not properly set. It should be set to 'AWS' or 'AZURE'.");
        if (cloudEnvironment == null) {
            throw environmentConfigurationException;
        } else if (cloudEnvironment.equals("AWS")) {
            environment = Environment.AWS;
        } else if (cloudEnvironment.equals("AZURE")) {
            environment = Environment.Azure;
        } else {
            throw environmentConfigurationException;
        }
    }

    public MetricRegistry getMetrics() {
        return metrics;
    }

    public Integer getUndertowIoThreadMultiplier() {
        return undertowIoThreadMultiplier;
    }

    public Integer getUndertowWorkerThreads() {
        return undertowWorkerThreads;
    }

    public Integer getUndertowSocketBacklog() {
        return undertowSocketBacklog;
    }

    public Integer getUndertowBufferSizeMultiplier() {
        return undertowBufferSizeMultiplier;
    }

    public String getAzureConnectionString() {
        return azureConnectionString;
    }

    public String getAzureProductsTable() {
        return azureProductsTable;
    }

    public String getAzurePartitionKey() {
        return azurePartitionKey;
    }

    public String getAwsAccessKey() {
        return awsAccessKey;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public String getAwsSecretKey() {
        return awsSecretKey;
    }
}