package com.cloudedjudgement.shared;

import com.google.protobuf.InvalidProtocolBufferException;
import net.openhft.chronicle.ExcerptAppender;
import net.openhft.chronicle.ExcerptTailer;
import net.openhft.chronicle.IndexedChronicle;
import net.openhft.chronicle.tools.ChronicleTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.function.Consumer;

import static com.cloudedjudgement.shared.messages.ProtoMessages.OrderMessage;

public class Journal {
    String basePath;
    ExcerptAppender appender;
    IndexedChronicle chronicle;
    final Logger logger = LoggerFactory.getLogger(Journal.class);

    public Journal(String name) {

        basePath = "/clouded-judgement/data/" + name;

        try {
            this.chronicle = new IndexedChronicle(basePath);
            logger.info("New event log chronicle created at {}", basePath);
            this.appender = new IndexedChronicle(basePath).createAppender();
            logger.info("New excerpt APPENDER created for chronicle at {}", basePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void log(byte[] messageBytes) {
        appender.startExcerpt(100);
        appender.write(messageBytes);
        appender.finish();
    }

    public void clear() {
        ChronicleTools.deleteOnExit(basePath);
    }

    public void consume(Consumer<OrderMessage> orderMessageConsumer) {
        consumeBytes(messageBytes -> {
            try {
                OrderMessage message = OrderMessage.parseFrom(messageBytes);
                orderMessageConsumer.accept(message);
            } catch (InvalidProtocolBufferException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void consumeBytes(Consumer<byte[]> byteConsumer) {
        ExcerptTailer tailer = null;
        try {
            tailer = this.chronicle.createTailer();
            logger.info("New excerpt TAILER created for chronicle at {}", basePath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        while (tailer.nextIndex()) {
            byte[] messageBytes = new byte[tailer.length()];
            tailer.read(messageBytes);
            tailer.finish();
            byteConsumer.accept(messageBytes);
        }
        tailer.close();
    }
}
