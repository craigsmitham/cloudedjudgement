/**
 * Sitewide logic here
 */

var cloudy = (function() {
	
	var init = function() {
		// Set active class on current nav bar item
		var path = window.location.pathname;
		if(path.indexOf('orders') > -1) {
			$("a[href='/orders']").parent().addClass("active");
		} else if(path.indexOf('products') > -1) {
			$("a[href='/products']").parent().addClass("active");
		} else if(path.indexOf('metrics') > -1) {
			$("a[href='/metrics']").parent().addClass("active");
		} 
	};
	
	$(document).ready(function() {
		init();
	});
}());
