package com.cloudedjudgement.web.undertow;

import java.io.File;
import java.net.URI;
import java.net.URL;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

public class WebsiteHandler implements HttpHandler {

	@Override
	public void handleRequest(HttpServerExchange exchange) throws Exception {
		URL path = this.getClass().getResource("../../../../templates/index.html");
		File file = new File(path.toURI());
		String html = Files.toString(file, Charsets.UTF_8);
		exchange.getResponseSender().send(html);
	}

}
