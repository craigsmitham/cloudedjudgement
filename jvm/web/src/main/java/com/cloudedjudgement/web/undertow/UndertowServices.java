package com.cloudedjudgement.web.undertow;

import com.cloudedjudgement.services.orderService.OrderService;
import com.cloudedjudgement.services.productService.ProductService;
import com.cloudedjudgement.shared.CloudedJudgementContext;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.handlers.proxy.SimpleProxyClientProvider;
import io.undertow.util.Headers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xnio.Options;

import java.net.URI;

import static io.undertow.Handlers.proxyHandler;

public class UndertowServices {
    final private Logger logger = LoggerFactory.getLogger(UndertowServices.class);
    final private CloudedJudgementContext context;
    final private OrderService orderService;
    final private ProductService productService;

    public UndertowServices(CloudedJudgementContext context, OrderService orderService, ProductService productService) {
        this.context = context;
        this.orderService = orderService;
        this.productService = productService;
    }

    public void start() {
        logger.info("Starting Undertow web service.");
        Integer bufferSize = 1024 * context.getUndertowBufferSizeMultiplier();
        logger.info("Undertow buffer size: {} (multiplier: {})", bufferSize, context.getUndertowBufferSizeMultiplier());
        Integer availableProcessors = Runtime.getRuntime().availableProcessors();
        Integer ioThreads = availableProcessors * context.getUndertowIoThreadMultiplier();
        logger.info("Undertow IO threads: {} (available processors: {}, thread multiplier: {})", ioThreads, availableProcessors, context.getUndertowIoThreadMultiplier());
        logger.info("Undertow socket backlog: {}", context.getUndertowSocketBacklog());
        logger.info("Undertow worker threads: {}", context.getUndertowWorkerThreads());

        SimpleProxyClientProvider websiteProxy = new SimpleProxyClientProvider(URI.create("http://localhost:4567"));

        Undertow.builder()
                .addHttpListener(8080, "0.0.0.0")
                .setBufferSize(bufferSize)
                .setIoThreads(ioThreads)
                .setSocketOption(Options.BACKLOG, context.getUndertowSocketBacklog())
                .setServerOption(UndertowOptions.ALWAYS_SET_KEEP_ALIVE, false)
                        // don't send a keep-alive header for HTTP/1.1 requests, as it
                        // is not required
                .setServerOption(UndertowOptions.ALWAYS_SET_DATE, true)
                .setHandler(
                        Handlers.header(
                                Handlers.path()
                                        .addPrefixPath("/", proxyHandler(websiteProxy))
                                        .addPrefixPath("/api/search-products",
                                                new SearchProductHandler(productService))
                                        .addPrefixPath(
                                                "/api/create-order",
                                                new CreateOrderHandler(orderService))
                                        .addPrefixPath("/api/add-item",
                                                new AddItemHandler(orderService))
                                        .addPrefixPath(
                                                "/api/submit-order",
                                                new SubmitOrderHandler(orderService)),
                                Headers.SERVER_STRING, "CJ"))
                .setWorkerThreads(context.getUndertowWorkerThreads())
                .build()
                .start();
    }
}
