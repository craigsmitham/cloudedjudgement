package com.cloudedjudgement.web.models;

import com.cloudedjudgement.services.orderService.OrderService;
import spark.ModelAndView;

import java.util.Set;

public class OrderListViewModel {
    Set<String> orderIds;

    OrderListViewModel(Set<String> orderIds) {
        this.orderIds = orderIds;
    }

    public static ModelAndView Build(OrderService orderService) {
        Set<String> orderIds = orderService.getOrderIds();
        OrderListViewModel viewModel = new OrderListViewModel(orderIds);
        return new ModelAndView(viewModel, "orderList.mustache");
    }
}

