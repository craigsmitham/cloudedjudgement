package com.cloudedjudgement.web.undertow;

import com.cloudedjudgement.services.orderService.OrderService;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SubmitOrderHandler implements HttpHandler {
    private OrderService orderService;
    final Logger logger = LoggerFactory.getLogger(SubmitOrderHandler.class);

    public SubmitOrderHandler(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        try {
            orderService.submitOrder(exchange.getQueryParameters().get("orderId").getFirst());
        } catch (Exception e) {
            logger.error("SubmitOrdreHandler error", e);
            exchange.setResponseCode(500);
            exchange.getResponseSender().send("OOPS");
        }
    }
}