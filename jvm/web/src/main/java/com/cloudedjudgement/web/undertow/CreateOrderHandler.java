package com.cloudedjudgement.web.undertow;

import com.cloudedjudgement.services.orderService.OrderService;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class CreateOrderHandler implements HttpHandler {
    final Logger logger = LoggerFactory.getLogger(CreateOrderHandler.class);
    private OrderService orderService;

    public CreateOrderHandler(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        try {
            String orderId = exchange.getQueryParameters().get("orderId").peekFirst();
            if (orderId != null) {
                orderService.createOrder(orderId);
            }
            else {
                orderService.createOrder();
            }

        } catch (Exception e) {
            logger.error("CreateOrderHandler error", e);
            exchange.setResponseCode(500);
            exchange.getResponseSender().send("OOPS");
        }
    }
}
