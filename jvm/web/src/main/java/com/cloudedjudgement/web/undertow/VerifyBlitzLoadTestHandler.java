package com.cloudedjudgement.web.undertow;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;

/**
 * Created by craig on 7/12/14.
 */
public class VerifyBlitzLoadTestHandler implements HttpHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        exchange.getResponseSender().send("42");
    }
}
