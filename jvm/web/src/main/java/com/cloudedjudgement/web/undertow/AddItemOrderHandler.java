package com.cloudedjudgement.web.undertow;

import com.cloudedjudgement.services.orderService.OrderService;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class AddItemHandler implements HttpHandler {
    private OrderService orderService;
    final Logger logger = LoggerFactory.getLogger(AddItemHandler.class);

    public AddItemHandler(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        try {
            orderService.addItemToOrder(getOrderId(exchange), getItemId(exchange));
        } catch (Exception e) {
            logger.error("AddItemHandler error", e);
            exchange.setResponseCode(500);
            exchange.getResponseSender().send("OOPS");
        }
    }

    private String getOrderId(HttpServerExchange exchange) {
        try {
            return exchange.getQueryParameters().get("orderId").getFirst();
        } catch (Exception e) {
            logger.error("Error getting orderId");
            throw e;
        }
    }

    private String getItemId(HttpServerExchange exchange) {
        try {
            return exchange.getQueryParameters().get("itemId").getFirst();
        } catch (Exception e) {
            logger.error("Error getting itemId");
            throw e;
        }
    }
}
