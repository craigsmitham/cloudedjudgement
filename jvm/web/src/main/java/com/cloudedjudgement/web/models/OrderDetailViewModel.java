package com.cloudedjudgement.web.models;

import com.cloudedjudgement.services.orderService.Order;
import com.cloudedjudgement.services.productService.Product;
import com.cloudedjudgement.services.productService.ProductService;
import spark.ModelAndView;

import java.util.ArrayList;

public class OrderDetailViewModel {
    Order order;
    ArrayList<Product> products;
    String query;

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }

    public OrderDetailViewModel(Order order) {
        this.order = order;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public static ModelAndView Build(Order order, String searchQuery, ProductService productService) {
        OrderDetailViewModel viewModel = new OrderDetailViewModel(order);
        viewModel.setQuery(searchQuery);
        if (searchQuery != null) {
            viewModel.setProducts(productService.search(searchQuery));
        }
        return new ModelAndView(viewModel, "orderDetail.mustache");
    }
}
