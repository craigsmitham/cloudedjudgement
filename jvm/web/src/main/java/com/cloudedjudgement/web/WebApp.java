package com.cloudedjudgement.web;

import com.amazonaws.services.sns.model.NotFoundException;
import com.cloudedjudgement.services.orderService.EventuallyPersistantInMemoryOrderService;
import com.cloudedjudgement.services.orderService.Order;
import com.cloudedjudgement.services.orderService.OrderService;
import com.cloudedjudgement.services.productService.Product;
import com.cloudedjudgement.services.productService.ProductService;
import com.cloudedjudgement.shared.CloudedJudgementContext;
import com.cloudedjudgement.web.models.OrderDetailViewModel;
import com.cloudedjudgement.web.models.OrderListViewModel;
import com.cloudedjudgement.web.models.ProductSearchResultsViewModel;
import com.cloudedjudgement.web.undertow.UndertowServices;
import com.codahale.metrics.json.MetricsModule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.ModelAndView;
import spark.template.mustache.MustacheTemplateEngine;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static spark.Spark.*;

public class WebApp {
    static final Logger logger = LoggerFactory.getLogger(WebApp.class);
    static CloudedJudgementContext context;
    static OrderService orderService;
    static ProductService productService;

    public static void main(String[] args) {
        logger.info("Starting application.");
        logger.info("Instantiating application context.");
        // Context contains configuration and metrics registry,
        // just about everything should have a reference to it
        context = new CloudedJudgementContext();
        orderService = EventuallyPersistantInMemoryOrderService.createAndSchedulePersistence(context);
        //orderService = CommandSourcedOrderService.create(context);
        productService = ProductService.getInstance(context);

        // Start web services
        new UndertowServices(context, orderService, productService).start();

        // Start web site
        initializeWebsite(orderService);
    }


    public static void initializeWebsite(OrderService orderService) {
        staticFileLocation("/public");

        exception(RuntimeException.class, (e, request, response) -> {
            response.status(500);
            response.body(Throwables.getStackTraceAsString(e));
        });

        exception(NotFoundException.class, (e, request, response) -> {
            response.status(404);
            response.body("Resource not found");
        });

        get("/", (req, res) -> {
            res.redirect("/orders");
            return res;
        });

        get("/orders", (req, res) -> OrderListViewModel.Build(orderService),
                new MustacheTemplateEngine());

        post("/orders", (req, res) -> {
            orderService.createOrder();
            res.redirect("/orders");
            return res;
        });

        get("/orders/detail", (req, res) -> {
            String orderId = req.queryParams("orderId");
            String searchQuery = req.queryParams("query");
            Order order = orderService.getOrder(orderId);
            if (order == null) {
                String notFoundMessage = "Order " + orderId + " not found";
                logger.error(notFoundMessage);
                throw new NotFoundException(notFoundMessage);
            }
            return OrderDetailViewModel.Build(order, searchQuery, productService);
        }, new MustacheTemplateEngine());

        post("/orders/addItem", (req, res) -> {
            String orderId = req.queryParams("orderId");
            orderService.addItemToOrder(orderId, req.queryParams("itemId"));
            String redirectPath = "/orders/detail?orderId=" + orderId;
            String query = req.queryParams("query");
            if (query != null)
                redirectPath = redirectPath + "&query=" + query;
            res.redirect(redirectPath);
            return res;
        });


        get("/products", (req, res) -> {
            res.redirect("/products/search");
            return res;
        });

        get("/products/search", (req, res) -> {
            return new ModelAndView(null, "productSearch.mustache");
        }, new MustacheTemplateEngine());

        post("/products/search", (req, res) -> {
            String nameOrSku = req.queryParams("query");
            ArrayList<Product> searchResults = new ArrayList<Product>();
            try {
                searchResults = productService.search(nameOrSku);
            } catch (Exception e1) {
                // TODO Auto-generated catch block
                // Consider throwing a runtime exception here:
                // throw new RuntimeException(e1);
                //-  Craig
                e1.printStackTrace();
            }
            return ProductSearchResultsViewModel.Build(searchResults);
        }, new MustacheTemplateEngine());

        post("/orders/submit", (req, res) -> {
            String orderId = req.queryParams("orderId");
            orderService.submitOrder(orderId);
            res.redirect("/orders/detail?orderId=" + orderId);
            return res;
        });

        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new MetricsModule(TimeUnit.MILLISECONDS,
                        TimeUnit.SECONDS, false));

        get("/metrics", (req, res) -> {
            try {
                return mapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(context.getMetrics());
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
