package com.cloudedjudgement.web.models;

import java.util.ArrayList;

import com.cloudedjudgement.services.productService.Product;

import spark.ModelAndView;

public class ProductSearchResultsViewModel {
	ArrayList<Product> products;
	
	public ProductSearchResultsViewModel(ArrayList<Product> products) {
        this.products = products;
	}

	public static ModelAndView Build(ArrayList<Product> products) {
		return new ModelAndView(new ProductSearchResultsViewModel(products), "productSearchResults.mustache");
	}

}
