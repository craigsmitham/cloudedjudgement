package com.cloudedjudgement.web.undertow;

import com.cloudedjudgement.services.productService.ProductService;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchProductHandler implements HttpHandler {
    private final ProductService theService;
    final Logger logger = LoggerFactory.getLogger(SearchProductHandler.class);

    public SearchProductHandler(ProductService productService) {
        theService = productService;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        try {
            String query = exchange.getQueryParameters().get("q").getFirst();
            String searchResults = ProductService.listToString(theService
                    .search(query));
            exchange.getResponseSender().send(searchResults);
        } catch (Exception e) {
            logger.error("SearchProductHandler error: " + exchange.getRequestURI(), e);
            exchange.setResponseCode(500);
            exchange.getResponseSender().send("OOPS");
        }
    }
}
